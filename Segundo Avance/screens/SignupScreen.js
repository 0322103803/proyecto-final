import React from 'react';
import { View, Text, Image,TextInput, TouchableOpacity } from 'react-native';
import { StatusBar } from 'expo-status-bar'; 
import Animated, { FadeIn, FadeInDown, FadeInUp } from 'react-native-reanimated';
import { useNavigation } from '@react-navigation/native';

export default function SignUpScreen() {
  const navigation = useNavigation();

  const handleSignUp = () => {
    navigation.navigate('Login');
  };

  return (
    <View className="bg-white h-full w-full">
      <StatusBar Style="light" />
      <Image className="h-full w-full absolute" source={require('../assets/images/background.png')} />

      {/* Lights */}
      <View className="flex-row justify-around w-full absolute">
        <Animated.Image entering={FadeInUp.delay(200).duration(1000).springify().damping(3)} className="h-[225] w-[90]" source={require('../assets/images/light.png')} />
        <Animated.Image entering={FadeInUp.delay(400).duration(1000).springify().damping(3)} className="h-[160] w-[65]" source={require('../assets/images/light.png')} />
      </View>

       {/* title and form */}
       <View className="h-full w-full flex justify-around pt-40 pb-0">
            <View className="flex items-center">
            <Animated.View entering={FadeInUp.duration(1000).springify()} style={{ alignItems: 'center', marginBottom: 40 }}>
          <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 51 }}>Sign Up</Text>
        </Animated.View>
                </View>
              <View className="flex items-center mx-1 space-y-1.5" >
                <Animated.View entering={FadeInDown.delay(600).duration(1000).springify()} className="bg-black/5 p-4 rounded-2xl w-full mb-1">
                <TextInput placeholder="Username" placeholderTextColor={'gray'}/>
                </Animated.View>
                <Animated.View entering={FadeInDown.delay(400).duration(1000).springify()} className="bg-black/5 p-4 rounded-2xl w-full mb-1">
                <TextInput placeholder="Email" placeholderTextColor={'gray'}/>
                </Animated.View>
                <Animated.View entering={FadeInDown.delay(400).duration(1000).springify()}className="bg-black/5 p-4 rounded-2xl w-full mb-1" >
                <TextInput placeholder="password" placeholderTextColor={'gray'} secureTextEntry/>
                </Animated.View>
                <Animated.View entering={FadeInDown.delay(600).duration(1000).springify()}className="bg-black/5 p-4 rounded-2xl w-full mb-1" >
                <TextInput placeholder="Phone" placeholderTextColor={'gray'} secureTextEntry/>
                </Animated.View>
                <Animated.View entering={FadeInDown.delay(200).duration(1000).springify()}className="bg-black/5 p-4 rounded-2xl w-full mb-1" >
                <TextInput placeholder="Address" placeholderTextColor={'gray'} secureTextEntry/>
                </Animated.View>
                <Animated.View entering={FadeInDown.delay(400).duration(1000).springify()} className="w-full">
                    <TouchableOpacity onPress={handleSignUp} className="w-full bg-sky-400 p-3 rounded-2xl mb-0">
                        <Text className="text-xl font-bold text-white text-center">SignUp</Text>
                    </TouchableOpacity>
                </Animated.View>
                <Animated.View entering={FadeInDown.delay(600).duration(1000).springify()} className="flex-row justify-center">
                    <Text>Already have an account? </Text>
                    <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                        <Text className="text-sky-600">Login</Text>
                    </TouchableOpacity>
                </Animated.View>
              </View>
            </View>
        </View>
    );      
}
